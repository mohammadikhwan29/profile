@extends('layout.welcome')

@section('content')

 <section id="pricing" class="pricing">
      <div class="container">

        <div class="section-title">
          <h2>Tarif Wisata Gronjong Wariti</h2>
          
        </div>

        <div class="row no-gutters">

          <div class="col-lg-4" data-aos="fade-up">
            <h3>Paket 1</h3>
            <h4>25<span>Anak-anak</span></h4>
            <ul>
              <li><i class="bx bx-check"></i> Kolam Renang</li>
              <li><i class="bx bx-check"></i> Mandi Bola</li>
              <li><i class="bx bx-check"></i> Flying Fox</li>
              <li><i class="bx bx-check"></i> ATV/Trail</li>
              <li><i class="bx bx-check"></i> Kereta Coster</li>
            </ul>
          </div>

          <div class="col-lg-4" data-aos="fade-up">
            <h3>Paket 2</h3>
            <h4>50k<span>Dewasa</span></h4>
            <ul>
              <li><i class="bx bx-check"></i> Perahu</li>
              <li><i class="bx bx-check"></i> Perahu Bebek</li>
              <li><i class="bx bx-check"></i> Arung Jeram</li>
              <li><i class="bx bx-check"></i> Tubing</li>
            </ul>
            
          </div>
          <div class="col-lg-4" data-aos="fade-up">
            <h3>Paket Karaoke</h3>
            <h4>30k<span>Dewasa</span></h4>
            <ul>
              <li><i class="bx bx-check"></i> Minum</li>
              <li><i class="bx bx-check"></i> Snack</li>
            </ul>
            
          </div>

        </div>

      </div>
    </section>
    @endsection