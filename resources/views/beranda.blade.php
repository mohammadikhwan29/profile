@extends('layout.welcome')

@section('content')

<section id="hero" class="d-flex align-items-center">

<div class="container">
  <div class="row">
    <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1" data-aos="fade-up">
      <div>
        <h1>Selamat Datang</h1>
        <h2>Di Desa Wisata Gronjong Wariti Mejono Kediri</h2>
      </div>
    </div>
    <div class="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img" data-aos="fade-up">
      <img src="{{ asset('templet/assets/img/awal.jpg')}}" class="img-fluid" alt="">
    </div>
  </div>
</div>

</section>
@endsection