@extends('layout.welcome')

 @section('content')

<section id="gallery" class="gallery">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Gallery</h2>
          <p>Gronjong Wariti</p>
        </div>

      </div>

      <div class="container-fluid" data-aos="fade-up">
        <div class="gallery-slider swiper">
          <div class="swiper-wrapper">
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-1.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-1.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-2.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-2.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-3.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-3.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-4.jpeg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-4.jpeg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-5.jpeg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-5.jpeg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-6.jpeg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-6.jpeg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-7.jpeg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-7.jpeg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-8.jpeg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-8.jpeg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-9.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-9.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-10.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-10.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-11.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-11.jpg')}}" class="img-fluid" alt=""></a></div>
            <div class="swiper-slide"><a href="{{ asset('templet/assets/img/gallery/gallery-12.jpg')}}" class="gallery-lightbox" data-gall="gallery-carousel"><img src="{{ asset('templet/assets/img/gallery/gallery-12.jpg')}}" class="img-fluid" alt=""></a></div>
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section>
    @endsection