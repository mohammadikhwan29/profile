@extends('layout.welcome')

@section('content')

 <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contact</h2>
          
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-6 info">
                <i class="bx bx-map"></i>
                <h4>Alamat</h4>
                <p>Mejono, Kec. Plemahan,<br>Kabupaten Kediri</p>
              </div>
              <div class="col-lg-6 info">
                <i class="bx bx-phone"></i>
                <h4>No/Wa</h4>
                <p>+6285782377495</p>
              </div>
              <div class="col-lg-6 info">
                <i class="bx bx-envelope"></i>
                <h4>Email</h4>
                <p>gronjongwariti@mejono.co.id</p>
              </div>
              <div class="col-lg-6 info">
                <i class="bx bx-time-five"></i>
                <h4>Jam Buka</h4>
                <p>Setiap Hari : 07.00 - 17.00</p>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <h2>*Note:</h2>
            <h1>Jika anda ingin menyewa tempat ini silahkan hubungi kontak disamping</h1>
          </div>

        </div>

      </div>
    </section>
    @endsection