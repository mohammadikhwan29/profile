 @extends('layout.welcome')

 @section('content')
 
 
 <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">

          <h2>Latar Belakang Gronjong Wariti</h2>
          <p>Gronjong Wariti sebelumnya adalah sebuah sungai mati dan bau. Selain itu, di sepanjang sungai ini juga terkenal sebagai tempat yang angker. Karena hal-hal itulah penduduk sekitar tidak ingin berlama-lama berada ditempat itu.
             Namun hal itu berubah 180 derajat, saat salah satu warganya yang bernama Riyadi berinisiatif mengajak beberapa orang untuk membersihkan sungai dengan alasan kebersihan lingkungan.
             Awalnya ditertawakan dan dicemooh, namun setelah sungai mulai terlihat bersih, barulah penduduk sekitar mulai peduli dengan menyumbang makanan-makanan kecil dan saling membantu proses pembersihannya.
             Hingga di suatu titik, mereka tidak hanya sekedar membersihkannya, tetapi juga menghias daerah aliran sungai Gronjong Wariti itu dan mendirikan banyak wahana tempat bermain. Kemudian berdirilah sebuah tempat wisata lokal baru yang ikonik, yang menjadi tumpuan ekonomi warga sekitarnya.</p>
        </div>


      </div>
    </section>
    @endsection